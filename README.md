### Инструкция
Установка зависимостей:
> php bin/console composer install

Создание БД, запуск миграций
> php bin/console doctrine:database:create  
> php bin/console doctrine:migrations:migrate  

Запуск фикстур
> php bin/console hautelook:fixtures:load

Генерация ключей для JWT
> mkdir config/jwt  
> openssl genrsa -out config/jwt/private.pem -aes256 4096  
> openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

Запуск сервера
> php bin/console server:run