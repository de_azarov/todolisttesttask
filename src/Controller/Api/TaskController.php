<?php declare(strict_types = 1);

namespace App\Controller\Api;

use App\Exception\AppTaskEntityNotFoundException;
use App\Exception\AppTaskValidatorException;
use App\Service\TaskService;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="api")
 *
 * @author Denis Azarov <denis@azarov.de>
 */
final class TaskController extends FOSRestController
{
    const ERROR_EMPTY_PROPERTIES = "You must pass values for all properties.";

    /**
     * @var TaskService
     */
    private $taskService;
    
    /**
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @Rest\Get("/tasks")
     */
    public function getTasks(): View
    {
        $tasks = $this->taskService->getAlltasks();

        return View::create($tasks, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/tasks/{id}")
     *
     * @param integer $id
     *
     * @return View
     *
     * @throws AppTaskEntityNotFoundException
     */
    public function getTask(int $id): View
    {
        $task = $this->taskService->getTask($id);

        return View::create($task, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/tasks")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postTask(Request $request): View
    {
        $task = $this->taskService->addTask($request->request->get('name'));

        return View::create($task, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/tasks/{id}")
     *
     * @param integer $id
     * @param Request $request
     *
     * @return View
     *
     * @throws AppTaskEntityNotFoundException
     */
    public function putTask(int $id, Request $request): View
    {
        $name = $request->request->get('name');
        $isCompleted = $request->request->get('isCompleted');

        if (!isset($name, $isCompleted)) {
            throw new AppTaskValidatorException(self::ERROR_EMPTY_PROPERTIES);
        }

        $isCompletedBool = filter_var($isCompleted, FILTER_VALIDATE_BOOLEAN);
        $task = $this->taskService->updateTask($id, $name, $isCompletedBool);

        return View::create($task, Response::HTTP_OK);
    }

    /**
     * @Rest\Patch("/tasks/{id}")
     *
     * @param integer $id
     * @param Request $request
     *
     * @return View
     *
     * @throws AppTaskEntityNotFoundException
     */
    public function patchTask(int $id, Request $request): View
    {
        $isCompleted = filter_var($request->request->get('isCompleted'), FILTER_VALIDATE_BOOLEAN);
        $task = $this->taskService->updateStatus($id, $isCompleted);

        return View::create($task, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Delete("/tasks/{id}")
     * 
     * @param integer $id
     * 
     * @return View
     */
    public function deleteTask(int $id): View
    {
        $this->taskService->deleteTask($id);

        return View::create([], Response::HTTP_NO_CONTENT);
    }
}
