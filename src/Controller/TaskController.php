<?php declare(strict_types = 1);

namespace App\Controller;

use App\Service\TaskService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Denis Azarov <denis@azarov.de>
 */
final class TaskController extends AbstractController
{
    /**
     * @var TaskService
     */
    private $taskService;

    /**
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @Route(path="/", name="task_get_all")
     *
     * @return Response
     */
    public function getAll(): Response
    {
        $tasks = $this->taskService->getAllTasks();

        return $this->render('task\list.html.twig', ['tasks' => $tasks]);
    }
}
