<?php declare(strict_types = 1);

namespace App\Exception;

use Symfony\Component\Validator\Exception\RuntimeException;

/**
 * @author Denis Azarov <denis@azarov.de>
 */
class AppTaskEntityNotFoundException extends RuntimeException
{

}
