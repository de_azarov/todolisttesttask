<?php declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Task|null find(mixed $id)
 *
 * @author Denis Azarov <denis@azarov.de>
 */
final class TaskRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Task::class);
    }

    /**
     * @param boolean|null $isCompleted
     */
    public function getAllByStatus(?bool $isCompleted)
    {

    }
}
