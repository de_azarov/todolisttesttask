<?php declare(strict_types = 1);

namespace App\Service;

use App\Entity\Task;
use App\Exception\AppTaskEntityNotFoundException;
use App\Exception\AppTaskValidatorException;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author Denis Azarov <denis@azarov.de>
 */
final class TaskService
{
    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var EntityManagerInterface
*/
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param TaskRepository $taskRepository
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(TaskRepository $taskRepository, EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->taskRepository = $taskRepository;
        $this->em = $entityManager;
        $this->validator = $validator;
    }

    /**
     * @param integer $id
     *
     * @return Task|null
     */
    public function getTask(int $id): ?Task
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new AppTaskEntityNotFoundException('Task with id '.$id.' does not exist!');
        }

        return $task;
    }

    /**
     * @return array|null
     */
    public function getAllTasks(): ?array
    {
        return $this->taskRepository->findAll();
    }

    /**
     * @param string $name
     *
     * @return Task
     */
    public function addTask(string $name): Task
    {
        $task = new Task();
        $task->setName($name);

        $validationErrors = $this->getValidationErrors($task);
        if ($validationErrors) {
            throw new AppTaskValidatorException($validationErrors);
        }

        $this->em->persist($task);
        $this->em->flush();

        return $task;
    }

    /**
     * @param integer $id
     * @param string  $name
     * @param boolean $isCompleted
     *
     * @return Task|null
     */
    public function updateTask(int $id, string $name, bool $isCompleted): ?Task
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new AppTaskEntityNotFoundException('Task with id '.$id.' does not exist!');
        }

        $task->setName($name);
        $task->setIsCompleted($isCompleted);

        $validationErrors = $this->getValidationErrors($task);
        if ($validationErrors) {
            throw new AppTaskValidatorException($validationErrors);
        }

        $this->em->flush();

        return $task;
    }

    /**
     * @param integer $id
     * @param boolean $isCompleted
     *
     * @return Task|null
     */
    public function updateStatus(int $id, bool $isCompleted): ?Task
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new AppTaskEntityNotFoundException('Task with id '.$id.' does not exist!');
        }

        $task->setIsCompleted($isCompleted);
        $this->em->flush();

        return $task;
    }

    /**
     * @param integer $id
     */
    public function deleteTask(int $id): void
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new AppTaskEntityNotFoundException('Task with id '.$id.' does not exist!');
        }

        $this->em->remove($task);
        $this->em->flush();
    }

    private function getValidationErrors(Task $task) {
       $errors = $this->validator->validate($task);

        if (count($errors)) {
            return (string) $errors;
        }

        return null;
    }
}